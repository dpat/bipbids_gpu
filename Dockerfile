FROM bids/base_fsl

ENV TERM="xterm"
#
# # Install python3, nibabel and git
# # run.py must need these for argument parsing and such
RUN apt-get update && \
apt-get install unzip && \
apt-get install -y git && \
apt-get install -y python3 python3-pip

RUN pip3 install numpy && \
pip3 install nibabel && \
apt-get remove -y python3-pip && \
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Add tools for running on HPC gpu node as singularity container

# Use ADD to fetch the remote zip files for CUDA 7.5 used on UofA HPC
COPY bedpostx_gpu.zip ${FSLDIR}
COPY probtrackx2_gpu.zip ${FSLDIR}
COPY eddy_cuda7.5 ${FSLDIR}

# Temporarily set WORKDIR to facilitate unzipping
WORKDIR ${FSLDIR}
# Most of these files go into ${FSLDIR}/bin
RUN unzip ${FSLDIR}/bedpostx_gpu.zip && \
unzip ${FSLDIR}/probtrackx2_gpu.zip
# LIBRARY files are alone in lib dir.  Copying them to bin to make
# sure they are accessible.  Linking may work and save space, but
# that is an optimization for later

RUN cp ${FSLDIR}/lib/*.so ${FSLDIR}/bin/ && \
mv ${FSLDIR}/probtrackx2_gpu ${FSLDIR}/bin/ && \
mv ${FSLDIR}/eddy_cuda7.5 ${FSLDIR}/bin/eddy_cuda

# Clean up zip files
RUN rm ${FSLDIR}/bedpostx_gpu.zip && rm ${FSLDIR}/probtrackx2_gpu.zip
# Reset WORKDIR to default
WORKDIR /
# Make the bip directory
RUN mkdir /usr/local/bip
ENV BIP_HOME=/usr/local/bip
ENV PATH=${BIP_HOME}:${PATH}

# these are really for the singularity container for the UofA HPC:
RUN mkdir /xdisk && \
mkdir /extra && \
mkdir /rsgrps

ENV PYTHONPATH=""

COPY run.py /run.py

COPY version /version
# git clone with a shallow git tree to reduce bloat
RUN git clone --depth 1 https://dpat@bitbucket.org/dpat/bipbids.git /usr/local/bip
# remove the shallow .git tree to save space
RUN rm -rf /usr/local/bip/.git

ENTRYPOINT ["/run.py"]
